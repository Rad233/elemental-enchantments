/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments;

import eu.midnightdust.lib.config.MidnightConfig;
import fr.creerio.elementalenchantments.registry.Curses;
import fr.creerio.elementalenchantments.registry.Enchantments;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.loader.api.FabricLoader;

public class ElementalEnchantments implements ModInitializer {

    public static final String TAG = "elementalenchantments";

    @Override
    public void onInitialize() {
        // Mod config
        if (FabricLoader.getInstance().isModLoaded("midnightlib"))
            MidnightConfig.init(TAG, Config.class);

        // Enchantments
        Enchantments.init();
        Curses.init();
    }
}
