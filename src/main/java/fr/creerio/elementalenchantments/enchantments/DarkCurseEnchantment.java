/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;

import static fr.creerio.elementalenchantments.Config.soundEffects;

/**
 * Curse of darkness
 * Damage taken boosted by day, equipped armor heals damage when taking damage at night
 * Armor enchantment
 * Incompatible w/ Curse of light
 */
public class DarkCurseEnchantment extends Enchantment {

    public static final String TAG = "dark_curse";

    public DarkCurseEnchantment() {
        super(Rarity.RARE, EnchantmentTarget.WEARABLE, new EquipmentSlot[] {EquipmentSlot.FEET, EquipmentSlot.LEGS, EquipmentSlot.CHEST, EquipmentSlot.HEAD});
    }

    @Override
    public int getMinPower(int level) {
        return 10;
    }

    @Override
    public int getMaxPower(int level) {
        return 30;
    }

    // Min/max level is already 1 by default

    @Override
    protected boolean canAccept(Enchantment other) {
        return !(other instanceof LightCurseEnchantment) && super.canAccept(other);
    }

    @Override
    public void onUserDamaged(LivingEntity user, Entity attacker, int level) {
        if (user.getWorld().isDay()) {
            getEquipment(user).forEach(((equipmentSlot, itemStack) -> {
                if (itemStack.isDamageable() && itemStack.getDamage() < itemStack.getMaxDamage()) {
                    itemStack.setDamage(itemStack.getDamage() - 2);
                }
            }));
            if (soundEffects && !user.getWorld().isClient) {
                user.getWorld().playSound(null, user.getBlockPos(), SoundEvents.BLOCK_AMETHYST_BLOCK_PLACE, SoundCategory.NEUTRAL, 0.75F, 1.0F);
            }
        }


        super.onUserDamaged(user, attacker, level);
    }

    @Override
    public boolean isCursed() {
        return true;
    }
}
