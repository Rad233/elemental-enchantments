/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.AxeItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SwordItem;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;

import static fr.creerio.elementalenchantments.Config.soundEffects;

/**
 * Light aspect enchantment
 * At day, damage boosted, at night durability damage is boosted
 * Sword/Axe only enchantment
 * Incompatible w/ Dark aspect
 */
public class LightAspectEnchantment extends Enchantment {

    public static final String TAG = "light_aspect";

    public LightAspectEnchantment() {
        super(Rarity.RARE, EnchantmentTarget.WEAPON, new EquipmentSlot[] {EquipmentSlot.MAINHAND});
    }

    public static float extractDamage(Item item) {
        if (item instanceof SwordItem swordItem)
            return swordItem.getAttackDamage();
        else if (item instanceof AxeItem axeItem)
            return axeItem.getAttackDamage();
        else
            return 0.0F;
    }

    @Override
    public int getMinPower(int level) {
        return 10 + 20 * (level - 1);
    }

    @Override
    public int getMaxPower(int level) {
        return super.getMinPower(level) + 50;
    }

    @Override
    public int getMaxLevel() {
        return 2;
    }

    @Override
    protected boolean canAccept(Enchantment other) {
        return !(other instanceof DarkAspectEnchantment) && super.canAccept(other);
    }

    @Override
    public boolean isAcceptableItem(ItemStack stack) {
        // Sword & Axe allowed, nothing else (except if extends the class)
        return super.isAcceptableItem(stack) || (stack.getItem() instanceof AxeItem);
    }

    @Override
    public void onTargetDamaged(LivingEntity user, Entity target, int level) {
        final Item usedItem = user.getMainHandStack().getItem();
        if (this.isAcceptableItem(user.getMainHandStack())) {
            final float baseDamage = extractDamage(usedItem) * level;
            if (target.getWorld().isNight()) {
                user.getMainHandStack().damage((int) baseDamage, user, null);
                if (soundEffects && !target.getWorld().isClient) {
                    target.getWorld().playSound(null, target.getBlockPos(), SoundEvents.ENCHANT_THORNS_HIT, SoundCategory.NEUTRAL, 0.75F, 1.0F);
                }
            }
        }

        super.onTargetDamaged(user, target, level);
    }
}
