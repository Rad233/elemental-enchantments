/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.KnockbackEnchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;

import static fr.creerio.elementalenchantments.Config.*;

/**
 * Curse of the wind
 * Knock-backs the attacked entity far, disables the sword's damage
 * Sword only enchantment
 * Incompatible w/ Wind aspect
 */
public class WindCurseEnchantment extends Enchantment {

    public static final String TAG = "wind_curse";

    public WindCurseEnchantment() {
        super(Rarity.RARE, EnchantmentTarget.WEAPON, new EquipmentSlot[] {EquipmentSlot.MAINHAND});
    }

    @Override
    public int getMinPower(int level) {
        return 10;
    }

    @Override
    public int getMaxPower(int level) {
        return 30;
    }

    // Min/max level is already 1 by default

    @Override
    protected boolean canAccept(Enchantment other) {
        return !(other instanceof KnockbackEnchantment) && !(other instanceof WindAspectEnchantment) && super.canAccept(other);
    }

    @Override
    public void onTargetDamaged(LivingEntity user, Entity target, int level) {
        final double xPow = (target.getX() - user.getX()) * windCursePowerFactor;
        final double yPow = (target.getY() - user.getY()) * windCursePowerFactor;
        final double zPow = (target.getZ() - user.getZ()) * windCursePowerFactor;

        target.addVelocity(xPow, yPow, zPow);

        // Force a packet to be sent between the target entity (in case of a player) and the server to apply the velocity properly
        target.velocityModified = true;

        // Server-sent particles, reveals the armor curse
        if (!user.getWorld().isClient) {
            final MinecraftServer srv = user.getServer();
            if (srv != null) {
                final ServerWorld world = srv.getWorld(user.getWorld().getRegistryKey());
                if (world != null) {
                    if (particles)
                        world.spawnParticles(ParticleTypes.CLOUD, target.getX(), target.getY(), target.getZ(), 5, 0, 1, 0, 1);

                    if (soundEffects)
                        world.playSound(null, target.getBlockPos(), SoundEvents.ENTITY_ENDER_DRAGON_FLAP, SoundCategory.PLAYERS, 0.5F, 2.0F);
                }
            }
        }

        super.onTargetDamaged(user, target, level);
    }

    @Override
    public boolean isCursed() {
        return true;
    }
}
