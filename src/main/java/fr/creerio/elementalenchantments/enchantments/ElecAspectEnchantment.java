/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.QuickChargeEnchantment;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.ItemStack;

import static fr.creerio.elementalenchantments.Config.elecAspectSpeedNerf;
import static fr.creerio.elementalenchantments.registry.Enchantments.ELEC_ASPECT;

/**
 * Lightning aspect enchantment
 * Projectiles shot are faster
 * Bow/Crossbow only enchantment
 * Incompatible w/ Quick charge & Curse of the earth
 */
public class ElecAspectEnchantment extends Enchantment {

    public static final String TAG = "elec_aspect";

    public ElecAspectEnchantment() {
        super(Rarity.RARE, EnchantmentTarget.BOW, new EquipmentSlot[] {EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND});
    }

    /**
     * Modifies the projectile's speed, if the shooter's weapon has the required enchantment
     *
     * @param shooter
     * Shooting entity
     *
     * @param baseSpeed
     * Base speed applied to the projectile
     *
     * @return Boosted speed or normal speed
     */
    public static float getSpeed(LivingEntity shooter, float baseSpeed) {
        if (baseSpeed == 0)
            return baseSpeed;

        final ItemStack handItem = shooter.getMainHandStack();
        final int elecAspectLvl = EnchantmentHelper.getLevel(ELEC_ASPECT, handItem);

        if (elecAspectLvl != 0)
            return baseSpeed * (float)(elecAspectLvl / elecAspectSpeedNerf);
        else
            return baseSpeed;
    }

    @Override
    public int getMinPower(int level) {
        return 10 + 20 * (level - 1);
    }

    @Override
    public int getMaxPower(int level) {
        return super.getMinPower(level) + 50;
    }

    @Override
    public int getMaxLevel() {
        return 2;
    }

    @Override
    protected boolean canAccept(Enchantment other) {
        return !(other instanceof QuickChargeEnchantment) && !(other instanceof EarthCurseEnchantment) && super.canAccept(other);
    }

    @Override
    public boolean isAcceptableItem(ItemStack stack) {
        // Bow & crossbow allowed, nothing else (except if extends the class)
        return stack.getItem() instanceof CrossbowItem || super.isAcceptableItem(stack);
    }
}
