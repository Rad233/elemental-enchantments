/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.world.ServerWorld;

import static fr.creerio.elementalenchantments.Config.particles;

/**
 * Curse of never-ending freezing
 * When cold, the cold never ends
 * Armor enchantment
 * Incompatible w/ Curse of never-ending fire & Curse of lightning
 */
public class FrostCurseEnchantment extends Enchantment {

    public static final String TAG = "frost_curse";

    public FrostCurseEnchantment() {
        super(Rarity.RARE, EnchantmentTarget.WEARABLE, new EquipmentSlot[] {EquipmentSlot.FEET, EquipmentSlot.LEGS, EquipmentSlot.CHEST, EquipmentSlot.HEAD});
    }

    @Override
    public int getMinPower(int level) {
        return 10;
    }

    @Override
    public int getMaxPower(int level) {
        return 30;
    }

    // Min/max level is already 1 by default

    @Override
    protected boolean canAccept(Enchantment other) {
        return !(other instanceof FireCurseEnchantment) && !(other instanceof ElecCurseEnchantment) && super.canAccept(other);
    }

    @Override
    public void onUserDamaged(LivingEntity user, Entity attacker, int level) {
        // Server-sent particles, reveals the armor curse
        if (particles && !user.getWorld().isClient) {
            final MinecraftServer srv = user.getServer();
            if (srv != null) {
                final ServerWorld world = srv.getWorld(user.getWorld().getRegistryKey());
                if (world != null) {
                    world.spawnParticles(ParticleTypes.SNOWFLAKE, user.getX(), user.getY(), user.getZ(), 15, 0.3, 0.5, 0.3, 0.04);
                }
            }
        }

        super.onUserDamaged(user, attacker, level);
    }

    @Override
    public boolean isCursed() {
        return true;
    }
}
