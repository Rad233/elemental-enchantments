/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.FireAspectEnchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.particle.BlockStateParticleEffect;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.math.Vec3i;

import static fr.creerio.elementalenchantments.Config.*;

/**
 * Earth aspect enchantment
 * Entities on ground are sent upwards
 * Sword only enchantment
 * Incompatible w/ Fire aspect & Frost aspect & Wind aspect
 */
public class EarthAspectEnchantment extends Enchantment {

    public static final String TAG = "earth_aspect";

    public EarthAspectEnchantment() {
        super(Rarity.RARE, EnchantmentTarget.WEAPON, new EquipmentSlot[] {EquipmentSlot.MAINHAND});
    }

    @Override
    public int getMinPower(int level) {
        return 10 + 20 * (level - 1);
    }

    @Override
    public int getMaxPower(int level) {
        return super.getMinPower(level) + 50;
    }

    @Override
    public int getMaxLevel() {
        return 2;
    }

    @Override
    protected boolean canAccept(Enchantment other) {
        return !(other instanceof FireAspectEnchantment) && !(other instanceof FrostAspectEnchantment) && !(other instanceof WindAspectEnchantment) && super.canAccept(other);
    }

    @Override
    public boolean isAcceptableItem(ItemStack stack) {
        // Sword & Axe allowed, nothing else (except if extends the class)
        return super.isAcceptableItem(stack) || (stack.getItem() instanceof AxeItem);
    }

    @Override
    public void onTargetDamaged(LivingEntity user, Entity target, int level) {

        if (target.isOnGround()) {
            // Velocity is different between player and monsters (more needed here than in other classes)
            if (target instanceof PlayerEntity)
                target.addVelocity(0.0F, earthAspectPowerFactorPlayer * level, 0.0F);
            else
                target.addVelocity(0.0F, earthAspectPowerFactorEntity * level, 0.0F);

            // Force a packet to be sent between the target entity (in case of a player obviously) and the server to apply the velocity properly
            target.velocityModified = true;

            // Server-sent particles & sound
            if (!user.getWorld().isClient) {
                final MinecraftServer srv = target.getServer();
                if (srv != null) {
                    final ServerWorld world = srv.getWorld(target.getWorld().getRegistryKey());
                    if (world != null) {
                        if (particles)
                            world.spawnParticles(new BlockStateParticleEffect(ParticleTypes.BLOCK, world.getBlockState(target.getBlockPos().subtract(new Vec3i(0, 1, 0)))), target.getX(), target.getY(), target.getZ(), 30, 0, 0, 0, 1);

                        if (soundEffects)
                            world.playSound(null, target.getBlockPos(), SoundEvents.BLOCK_GRASS_BREAK, SoundCategory.BLOCKS, 0.75F, 0.6F);
                    }
                }
            }
        }

        super.onTargetDamaged(user, target, level);
    }
}
