/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.FireAspectEnchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;

import static fr.creerio.elementalenchantments.Config.soundEffects;
import static fr.creerio.elementalenchantments.Config.windAspectPowerNerf;

/**
 * Wind aspect enchantment
 * Knock-backs the attacked entity to the attacker
 * Sword only enchantment
 * Incompatible w/ Fire aspect & Frost aspect & Earth aspect
 */
public class WindAspectEnchantment extends Enchantment {

    public static final String TAG = "wind_aspect";

    /**
     * Power nerf applied to the calculated velocity, making it less powerful
     */
    private static final double POWER_NERF = 15;

    public WindAspectEnchantment() {
        super(Rarity.RARE, EnchantmentTarget.WEAPON, new EquipmentSlot[] {EquipmentSlot.MAINHAND});
    }

    @Override
    public int getMinPower(int level) {
        return 10 + 20 * (level - 1);
    }

    @Override
    public int getMaxPower(int level) {
        return super.getMinPower(level) + 50;
    }

    @Override
    public int getMaxLevel() {
        return 2;
    }

    @Override
    protected boolean canAccept(Enchantment other) {
        return !(other instanceof FireAspectEnchantment) && !(other instanceof FrostAspectEnchantment) && !(other instanceof EarthAspectEnchantment) && super.canAccept(other);
    }

    @Override
    public void onTargetDamaged(LivingEntity user, Entity target, int level) {
        final double xPow = (user.getX() - target.getX())/ windAspectPowerNerf * level;
        final double yPow = (user.getY() - target.getY())/ windAspectPowerNerf * level;
        final double zPow = (user.getZ() - target.getZ())/ windAspectPowerNerf * level;

        target.addVelocity(xPow, yPow, zPow);

        // Force a packet to be sent between the target entity (in case of a player) and the server to apply the velocity properly
        target.velocityModified = true;

        if (soundEffects && !user.getWorld().isClient) {
            user.getWorld().playSound(null, user.getBlockPos(), SoundEvents.ENTITY_ENDER_DRAGON_FLAP, SoundCategory.PLAYERS, 0.5F, 2.0F);
        }

        super.onTargetDamaged(user, target, level);
    }
}
