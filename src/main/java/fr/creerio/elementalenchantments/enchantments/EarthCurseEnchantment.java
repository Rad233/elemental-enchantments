/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityGroup;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.CrossbowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;

import static fr.creerio.elementalenchantments.Config.earthCurseDamage;
import static fr.creerio.elementalenchantments.Config.soundEffects;
import static fr.creerio.elementalenchantments.registry.Curses.EARTH_CURSE;

/**
 * Curse of the earth
 * Arrow shot have no speed, gives the bow/crossbow some sword damage
 * Bow/Crossbow only enchantment
 * Incompatible w/ Lightning aspect
 */
public class EarthCurseEnchantment extends Enchantment {

    public static final String TAG = "earth_curse";

    public EarthCurseEnchantment() {
        super(Rarity.RARE, EnchantmentTarget.BOW, new EquipmentSlot[] {EquipmentSlot.MAINHAND});
    }

    /**
     * Modifies the projectile's speed, if the shooter's weapon has the required enchantment
     *
     * @param shooter
     * Shooting entity
     *
     * @param baseSpeed
     * Base speed applied to the projectile
     *
     * @return Speed at 0 or normal speed
     */
    public static float getSpeed(LivingEntity shooter, float baseSpeed) {
        final ItemStack handItem = shooter.getMainHandStack();
        final int curseLvl = EnchantmentHelper.getLevel(EARTH_CURSE, handItem);

        if (curseLvl != 0)
            return 0.0F;
        else
            return baseSpeed;
    }

    @Override
    public int getMinPower(int level) {
        return 10;
    }

    @Override
    public int getMaxPower(int level) {
        return 30;
    }

    // Min/max level is already 1 by default

    @Override
    public float getAttackDamage(int level, EntityGroup group) {
        return earthCurseDamage + super.getAttackDamage(level, group);
    }

    @Override
    protected boolean canAccept(Enchantment other) {
        return !(other instanceof ElecAspectEnchantment) && super.canAccept(other);
    }

    @Override
    public boolean isAcceptableItem(ItemStack stack) {
        // Bow & crossbow allowed, nothing else (except if extends the class)
        return stack.getItem() instanceof CrossbowItem || super.isAcceptableItem(stack);
    }

    @Override
    public void onTargetDamaged(LivingEntity user, Entity target, int level) {
        if (soundEffects && !user.getWorld().isClient) {
            user.getWorld().playSound(null, user.getBlockPos(), SoundEvents.ENTITY_PLAYER_ATTACK_SWEEP, SoundCategory.PLAYERS, 0.75F, 1.0F);
        }

        super.onTargetDamaged(user, target, level);
    }

    @Override
    public boolean isCursed() {
        return true;
    }
}
