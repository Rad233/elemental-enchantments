/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.FireAspectEnchantment;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;

import static fr.creerio.elementalenchantments.Config.frostAspectFrozenTickTime;
import static fr.creerio.elementalenchantments.Config.soundEffects;

/**
 * Frost aspect enchantment
 * Same properties that of the fire aspect enchantment but w/ cold (snow powder effect)
 * Sword only enchantment
 * Incompatible w/ Fire aspect & Wind aspect & Earth aspect
 */
public class FrostAspectEnchantment extends Enchantment {

    public static final String TAG = "frost_aspect";

    public FrostAspectEnchantment() {
        super(Rarity.RARE, EnchantmentTarget.WEAPON, new EquipmentSlot[] {EquipmentSlot.MAINHAND});
    }

    @Override
    public int getMinPower(int level) {
        return 10 + 20 * (level - 1);
    }

    @Override
    public int getMaxPower(int level) {
        return super.getMinPower(level) + 50;
    }

    @Override
    public int getMaxLevel() {
        return 2;
    }

    @Override
    protected boolean canAccept(Enchantment other) {
        return !(other instanceof FireAspectEnchantment) && !(other instanceof WindAspectEnchantment) && !(other instanceof EarthAspectEnchantment) && super.canAccept(other);
    }

    @Override
    public void onTargetDamaged(LivingEntity user, Entity target, int level) {
        if (target instanceof LivingEntity) {
            target.setFrozenTicks(frostAspectFrozenTickTime * level);
            if (soundEffects && !user.getWorld().isClient) {
                user.getWorld().playSound(null, user.getBlockPos(), SoundEvents.BLOCK_SNOW_HIT, SoundCategory.NEUTRAL, 0.75F, 1.0F);
            }
        }

        super.onTargetDamaged(user, target, level);
    }
}
