/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.enchantments;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.*;

/**
 * Curse of lightning
 * On thunderstorms, when hit, lightning falls on the entity
 * Armor enchantment
 * Incompatible w/ Curse of never-ending fire & Curse of never-ending freezing
 */
public class ElecCurseEnchantment extends Enchantment {

    public static final String TAG = "elec_curse";

    public ElecCurseEnchantment() {
        super(Rarity.RARE, EnchantmentTarget.WEARABLE, new EquipmentSlot[] {EquipmentSlot.FEET, EquipmentSlot.LEGS, EquipmentSlot.CHEST, EquipmentSlot.HEAD});
    }

    @Override
    public int getMinPower(int level) {
        return 10;
    }

    @Override
    public int getMaxPower(int level) {
        return 30;
    }

    // Min/max level is already 1 by default

    @Override
    protected boolean canAccept(Enchantment other) {
        return !(other instanceof FireCurseEnchantment) && !(other instanceof FrostCurseEnchantment) && super.canAccept(other);
    }

    @Override
    public void onUserDamaged(LivingEntity user, Entity attacker, int level) {
        if (user.getWorld().isThundering()) {
            final LightningEntity lightning = new LightningEntity(EntityType.LIGHTNING_BOLT, user.getWorld());
            lightning.setPosition(user.getPos());
            user.getWorld().spawnEntity(lightning);
        }

        super.onUserDamaged(user, attacker, level);
    }

    @Override
    public boolean isCursed() {
        return true;
    }
}
