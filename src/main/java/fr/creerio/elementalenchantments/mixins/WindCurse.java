/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.mixins;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyArg;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import static fr.creerio.elementalenchantments.registry.Curses.WIND_CURSE;

/**
 * Extends the Entity class to disable the damage taken when the damage method is called
 */
@Mixin(LivingEntity.class)
public abstract class WindCurse extends Entity {

    protected WindCurse(EntityType<?> type, World world) {
        super(type, world);
    }

    private boolean disableDamage = false;

    /**
     * Check for potential damage set to 0
     *
     * @param source
     * @param amount
     * @param cir
     */
    @Inject(method = "damage", at = @At("HEAD"))
    private void checkForDmg(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (cir.isCancelled() || source.getAttacker() == null)
            return;

        if (source.getAttacker() instanceof LivingEntity attacker && EnchantmentHelper.getLevel(WIND_CURSE, attacker.getMainHandStack()) > 0)
            disableDamage = true;
    }

    /**
     * Set damage at zero if needed
     *
     * @param amount
     * @return
     */
    @ModifyArg(method = "damage", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/LivingEntity;applyDamage(Lnet/minecraft/entity/damage/DamageSource;F)V"), index = 1)
    private float modifyDmg(float amount) {
        final float dmg = disableDamage ? 0 : amount;

        if (disableDamage)
            disableDamage = false;

        return dmg;
    }
}
