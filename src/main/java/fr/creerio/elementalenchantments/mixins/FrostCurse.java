/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.mixins;

import fr.creerio.elementalenchantments.enchantments.FrostCurseEnchantment;
import net.minecraft.block.BlockState;
import net.minecraft.block.PowderSnowBlock;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static fr.creerio.elementalenchantments.Config.frostCurseFrozenTickTime;
import static fr.creerio.elementalenchantments.registry.Curses.FROST_CURSE;

/**
 * Extends the Entity class to set the entity on a never-ending freezing when the tick method is called
 */
@Mixin(LivingEntity.class)
public abstract class FrostCurse extends Entity {

    @Shadow
    @Final
    private DefaultedList<ItemStack> syncedArmorStacks;

    /**
     * Storage of if the entity's armor set contains the required curse
     */
    private boolean hasFrostCurse;

    protected FrostCurse(EntityType<?> type, World world) {
        super(type, world);
    }

    /**
     * Checks if the entity has the required curse enchantment
     *
     * @param slot
     * @param armor
     * @param ci
     */
    @Inject(method = "setSyncedArmorStack", at = @At("TAIL"))
    private void checkArmor(EquipmentSlot slot, ItemStack armor, CallbackInfo ci) {
        if (!ci.isCancelled())
            hasFrostCurse = syncedArmorStacks.stream().anyMatch((itemStack -> EnchantmentHelper.get(itemStack).keySet().stream().anyMatch((FrostCurseEnchantment.class::isInstance))));
    }

    /**
     * Completes the tick method to freeze the entity if they are on freezing already
     * Doing this isn't optimized, as the tick method is called a lot.
     *
     * @param ci
     */
    @Inject(method = "tick", at = @At("HEAD"))
    private void applyFreeze(CallbackInfo ci) {
        if (!ci.isCancelled() && getFrozenTicks() > 1 && hasFrostCurse)
            setFrozenTicks(frostCurseFrozenTickTime);
    }
}

/**
 * Completes the onEntityCollision method of PowderSnowClass to freeze instantly the frost cursed entity and damage it
 * Also stops fire
 */
@Mixin(PowderSnowBlock.class)
class FrostCurseOnBlock {

    @Inject(method = "onEntityCollision", at = @At("TAIL"))
    private void applyFreeze(BlockState state, World world, BlockPos pos, Entity entity, CallbackInfo ci) {
        if (ci.isCancelled())
            return;

        final boolean hasFrostCurse = usesCursedArmor(entity);

        // Freeze + damage if has curse
        if (entity instanceof LivingEntity && hasFrostCurse) {
            entity.setFrozenTicks(frostCurseFrozenTickTime);
            entity.damage(entity.getWorld().getDamageSources().freeze(), 1f);
        }
    }

    /**
     * Checks if a given entity uses frost cursed equipment
     *
     * @param entity
     * Entity to check
     */
    @Unique
    private boolean usesCursedArmor(Entity entity) {
        boolean hasFrostCursedArmor = false;
        final Iterable<ItemStack> equippedItems = entity.getArmorItems();
        for (ItemStack itemStack : equippedItems) {
            // Find the cursed armor, if there is any we can leave the loop to gain efficiency
            if (EnchantmentHelper.getLevel(FROST_CURSE, itemStack) > 0) {
                hasFrostCursedArmor = true;
                break;
            }
        }
        return hasFrostCursedArmor;
    }
}