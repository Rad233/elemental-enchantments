/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.mixins;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import static fr.creerio.elementalenchantments.Config.darkAspectPowerNerf;
import static fr.creerio.elementalenchantments.enchantments.LightAspectEnchantment.extractDamage;
import static fr.creerio.elementalenchantments.registry.Enchantments.DARK_ASPECT;

/**
 * Extends the Entity class to boost the damage taken when the damage method is called
 */
@Mixin(LivingEntity.class)
public abstract class DarkAspect extends Entity {

    private float baseWeaponDamage = 0.0F;
    private int darkAspectLevel = 0;

    protected DarkAspect(EntityType<?> type, World world) {
        super(type, world);
    }

    /**
     * Checks the level of dark aspect on the attacker's weapon & get the weapon's attack damage
     *
     * @param source
     * @param amount
     * @param cir
     */
    @Inject(method = "damage", at = @At("HEAD"))
    private void checkForDarkAspectBoost(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (cir.isCancelled() || this.getWorld().isDay() || source.getAttacker() == null) {
            baseWeaponDamage = 0.0F;
            darkAspectLevel = 0;
            return;
        }

        if (source.getAttacker() instanceof LivingEntity attacker) {
            final ItemStack attackerWeapon = attacker.getMainHandStack();
            baseWeaponDamage = extractDamage(attackerWeapon.getItem());
            darkAspectLevel = EnchantmentHelper.getLevel(DARK_ASPECT, attackerWeapon);
        }
    }

    /**
     * Damage boost by night
     *
     * @param value
     * Base damage
     *
     * @return Base damage or higher one if the attacker has a dark aspect enchanted weapon
     */
    @ModifyVariable(method = "damage", at = @At("HEAD"), index = 2, argsOnly = true)
    private float modifyDmg(float value) {
        return darkAspectLevel > 0 ? value + (baseWeaponDamage * darkAspectLevel / darkAspectPowerNerf) : value;
    }
}
