/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.mixins;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import static fr.creerio.elementalenchantments.Config.fireCurseOnFireTime;
import static fr.creerio.elementalenchantments.registry.Curses.FIRE_CURSE;

/**
 * Extends the Entity class to set the entity on a never-ending fire when the damage method is called
 * Also stops cold
 */
@Mixin(LivingEntity.class)
public abstract class FireCurse extends Entity {

    @Shadow
    @Final
    private DefaultedList<ItemStack> syncedArmorStacks;

    /**
     * Storage of if the entity's armor set contains the required curse
     */
    private boolean hasFireCurse;

    protected FireCurse(EntityType<?> type, World world) {
        super(type, world);
    }

    @Inject(method = "setSyncedArmorStack", at = @At("TAIL"))
    private void checkArmor(EquipmentSlot slot, ItemStack armor, CallbackInfo ci) {
        hasFireCurse = syncedArmorStacks.stream().anyMatch((itemStack -> EnchantmentHelper.getLevel(FIRE_CURSE, itemStack) > 0));
    }

    /**
     * Completes the damage method to set the entity on fire if they are on fire
     *
     * @param source
     * @param amount
     * @param cir
     */
    @Inject(method = "damage", at = @At("HEAD"))
    private void applyFire(DamageSource source, float amount, CallbackInfoReturnable<Boolean> cir) {
        if (cir.isCancelled())
            return;

        // Never-ending fire
        if (isOnFire() && hasFireCurse)
            setOnFireFor(fireCurseOnFireTime);
    }

}
