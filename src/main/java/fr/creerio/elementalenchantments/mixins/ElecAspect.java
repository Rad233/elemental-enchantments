/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.mixins;

import fr.creerio.elementalenchantments.enchantments.ElecAspectEnchantment;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.BowItem;
import net.minecraft.item.CrossbowItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyArgs;
import org.spongepowered.asm.mixin.injection.invoke.arg.Args;

/**
 * Extends the BowItem class to modify the projectile speed w/ the onStoppedUsing method
 */
@Mixin(BowItem.class)
class ElecAspect {

    @ModifyArgs(method = "onStoppedUsing", at = @At(value = "INVOKE", target = "Lnet/minecraft/entity/projectile/PersistentProjectileEntity;setVelocity(Lnet/minecraft/entity/Entity;FFFFF)V"))
    private void modifySpeed(Args args) {
        if (args.get(0) instanceof LivingEntity shooter) {
            args.set(4, ElecAspectEnchantment.getSpeed(shooter, args.get(4)));
        }
    }
}

/**
 * Extends the CrossbowItem class to modify the projectile speed w/ the use method
 */
@Mixin(CrossbowItem.class)
class ElecAspectCrossbow {

    @ModifyArgs(method = "use", at = @At(value = "INVOKE", target = "Lnet/minecraft/item/CrossbowItem;shootAll(Lnet/minecraft/world/World;Lnet/minecraft/entity/LivingEntity;Lnet/minecraft/util/Hand;Lnet/minecraft/item/ItemStack;FF)V"))
    private void modifySpeed(Args args) {
        args.set(4, ElecAspectEnchantment.getSpeed(args.get(1), args.get(4)));
    }
}