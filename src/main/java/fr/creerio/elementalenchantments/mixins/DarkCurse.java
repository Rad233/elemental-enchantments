/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.mixins;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import static fr.creerio.elementalenchantments.Config.darkCursePowerNerf;
import static fr.creerio.elementalenchantments.registry.Curses.DARK_CURSE;

/**
 * Extends the Entity class to boost the damage taken when the damage method is called
 */
@Mixin(LivingEntity.class)
public abstract class DarkCurse extends Entity {

    @Shadow
    @Final
    private DefaultedList<ItemStack> syncedArmorStacks;

    /**
     * Storage of if the entity's armor set contains the required curse
     */
    private boolean hasDarkCurse;

    protected DarkCurse(EntityType<?> type, World world) {
        super(type, world);
    }

    @Inject(method = "setSyncedArmorStack", at = @At("TAIL"))
    private void checkArmor(EquipmentSlot slot, ItemStack armor, CallbackInfo ci) {
        if (!ci.isCancelled())
            hasDarkCurse = syncedArmorStacks.stream().anyMatch((itemStack -> EnchantmentHelper.getLevel(DARK_CURSE, itemStack) > 0));
    }

    /**
     * Damage boost by day w/ cursed armor
     *
     * @param value
     * @return
     */
    @ModifyVariable(method = "damage", at = @At("HEAD"), index = 2, argsOnly = true)
    private float modifyDmg(float value) {
        return this.getWorld().isNight() && hasDarkCurse ? value + value / darkCursePowerNerf : value;
    }
}
