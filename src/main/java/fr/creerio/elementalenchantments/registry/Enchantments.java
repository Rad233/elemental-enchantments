/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.registry;

import fr.creerio.elementalenchantments.ElementalEnchantments;
import fr.creerio.elementalenchantments.enchantments.*;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class Enchantments {
    // Sword
    public static final Enchantment FROST_ASPECT = new FrostAspectEnchantment();
    public static final Enchantment WIND_ASPECT = new WindAspectEnchantment();
    public static final Enchantment EARTH_ASPECT = new EarthAspectEnchantment();

    // Sword & Axe
    public static final Enchantment DARK_ASPECT = new DarkAspectEnchantment();
    public static final Enchantment LIGHT_ASPECT = new LightAspectEnchantment();

    // Bow/Crossbow
    public static final Enchantment ELEC_ASPECT = new ElecAspectEnchantment();

    /**
     * Static class, gives enchantments
     */
    private Enchantments() {}

    /**
     * Init registry
     */
    public static void init() {
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, DarkAspectEnchantment.TAG), DARK_ASPECT);
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, EarthAspectEnchantment.TAG), EARTH_ASPECT);
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, ElecAspectEnchantment.TAG), ELEC_ASPECT);
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, FrostAspectEnchantment.TAG), FROST_ASPECT);
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, LightAspectEnchantment.TAG), LIGHT_ASPECT);
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, WindAspectEnchantment.TAG), WIND_ASPECT);
    }
}
