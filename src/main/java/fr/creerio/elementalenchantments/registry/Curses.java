/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments.registry;

import fr.creerio.elementalenchantments.ElementalEnchantments;
import fr.creerio.elementalenchantments.enchantments.*;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class Curses {
    // Sword
    public static final Enchantment WIND_CURSE = new WindCurseEnchantment();

    // Bow/Crossbow
    public static final Enchantment EARTH_CURSE = new EarthCurseEnchantment();

    // Armor
    public static final Enchantment DARK_CURSE = new DarkCurseEnchantment();
    public static final Enchantment ELEC_CURSE = new ElecCurseEnchantment();
    public static final Enchantment FIRE_CURSE = new FireCurseEnchantment();
    public static final Enchantment FROST_CURSE = new FrostCurseEnchantment();
    public static final Enchantment LIGHT_CURSE = new LightCurseEnchantment();

    /**
     * Static class, gives curses
     */
    private Curses() {}

    /**
     * Init registry
     */
    public static void init() {
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, DarkCurseEnchantment.TAG), DARK_CURSE);
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, EarthCurseEnchantment.TAG), EARTH_CURSE);
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, ElecCurseEnchantment.TAG), ELEC_CURSE);
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, FireCurseEnchantment.TAG), FIRE_CURSE);
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, FrostCurseEnchantment.TAG), FROST_CURSE);
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, LightCurseEnchantment.TAG), LIGHT_CURSE);
        Registry.register(Registries.ENCHANTMENT, new Identifier(ElementalEnchantments.TAG, WindCurseEnchantment.TAG), WIND_CURSE);
    }
}
