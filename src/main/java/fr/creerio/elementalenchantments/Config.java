/**
 * Elemental Enchantments - A mod adding elemental enchantments
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package fr.creerio.elementalenchantments;

import eu.midnightdust.lib.config.MidnightConfig;

/**
 * Config entries available for the mod
 */
public class Config extends MidnightConfig {

    @Comment(centered = true)
    public static Comment general;

    @Entry()
    public static boolean soundEffects = true;

    @Entry()
    public static boolean particles = true;

    @Comment(centered = true)
    public static Comment sword;

    @Entry(isSlider = true, min = 0.01, max = 1.0)
    public static double earthAspectPowerFactorPlayer = 0.175;

    @Entry(isSlider = true, min = 0.01, max = 1.0)
    public static double earthAspectPowerFactorEntity = 0.05;

    @Entry(isSlider = true, min = 50, max = 200)
    public static int frostAspectFrozenTickTime = 150;

    @Entry(isSlider = true, min = 1, max = 20)
    public static int windAspectPowerNerf = 15;

    @Entry(isSlider = true, min = 1.0, max = 2.0)
    public static double windCursePowerFactor = 1.25;

    @Comment(centered = true)
    public static Comment swordAndAxe;

    @Entry(isSlider = true, min = 1, max = 5)
    public static float darkAspectPowerNerf = 3F;

    @Entry(isSlider = true, min = 1, max = 5)
    public static float lightAspectPowerNerf = 3F;

    @Comment(centered = true)
    public static Comment bowAndCrossbow;

    @Entry(isSlider = true, min = 1, max = 2)
    public static double elecAspectSpeedNerf = 1.25;

    @Entry(isSlider = true, min = 1, max = 6)
    public static float earthCurseDamage = 2.5F;

    @Comment(centered = true)
    public static Comment armor;

    @Entry(isSlider = true, min = 1, max = 5)
    public static float darkCursePowerNerf = 3F;

    @Entry(isSlider = true, min = 1, max = 10)
    public static int fireCurseOnFireTime = 2;

    @Entry(isSlider = true, min = 100, max = 500)
    public static int frostCurseFrozenTickTime = 300;

    @Entry(isSlider = true, min = 1, max = 5)
    public static float lightCursePowerNerf = 3F;
}
